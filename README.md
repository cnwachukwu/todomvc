
End to End Testing w/ Cypress.io and GitLab
======

Intro
===
This project highlights the various ways to incorporate an automated testing framework such as Cypress.io into the GitLab CI workflow. The web application that test cases will be run against is a To-Do list web app built in React. 

App Preview
===
![To-Do App](assets/todo_app.png)

Using Cypress in CI
===
Installing Cypress
> npm install cypress

Run Cypress in CI(Headless mode, no browser)
> cypress run

About Cypress.io
===
Learn more about [Cypress.io](https://docs.cypress.io/guides/overview/why-cypress.html#In-a-nutshell) tool.

This project extends the [existing repo](https://gitlab.com/bahmutov/todomvc) by the Cypress team


